from django.db import models
from django.core.validators import FileExtensionValidator

class FileUpload(models.Model):
	description = models.CharField(max_length = 500)
	file = models.FileField(validators = [FileExtensionValidator(allowed_extensions=['csv', 'xls'])])