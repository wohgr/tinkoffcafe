from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('data_analysis.urls'))
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, documrnt_root = settings.MEDIA_ROOT)
	urlpatterns += static(settings.STATIC_URL, documrnt_root = settings.STATIC_ROOT)